package com.example.demo.repositories;

import com.example.demo.core.Gare;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.CrudRepositoryExtensionsKt;
import org.springframework.stereotype.Repository;

@Repository
public interface GareRepository extends CrudRepository<Gare, String> {

}
