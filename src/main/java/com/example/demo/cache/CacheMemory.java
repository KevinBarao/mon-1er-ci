package com.example.demo.cache;

import org.springframework.stereotype.Service;

@Service
public class CacheMemory<T> implements CustomCache<T> {

    private T cache;

    public T getCache() {
        return cache;
    }

    public void updateCache(T cache) {
        this.cache = cache;
    }

    public void invalidate() {
        this.cache = null;
    }
}

