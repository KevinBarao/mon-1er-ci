package com.example.demo.cache;

import org.springframework.stereotype.Service;

public interface CustomCache<T> {
    public T getCache();
    public void updateCache(T cache);
    public void invalidate();

}
