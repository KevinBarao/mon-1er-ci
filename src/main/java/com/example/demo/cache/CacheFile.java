package com.example.demo.cache;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
@Primary
public class CacheFile<T> implements CustomCache<T> {

    @Autowired
    private ObjectMapper mapper;
    private T cache;

    public T getCache() {
        return null;
    }

    public void updateCache(T cache) {
        try {
            mapper.writeValue(new File("target/cache.json"), cache);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void invalidate() {
        this.cache = null;
    }
}
