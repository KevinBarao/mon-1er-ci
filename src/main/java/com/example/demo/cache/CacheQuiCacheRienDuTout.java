package com.example.demo.cache;

import org.springframework.stereotype.Service;

@Service
public class CacheQuiCacheRienDuTout<T> implements CustomCache<T>{
    @Override
    public T getCache() {
        return null;
    }

    @Override
    public void updateCache(T cache) {
        System.out.println("Cache inactif");
    }

    @Override
    public void invalidate() {
        System.out.println("Cache inactif");
    }
}
