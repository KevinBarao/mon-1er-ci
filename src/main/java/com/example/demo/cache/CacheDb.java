package com.example.demo.cache;

import com.example.demo.core.Gare;
import com.example.demo.repositories.GareRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class CacheDb<T> implements CustomCache<T> {

    @Autowired
    private CrudRepository repo;
    private T cache;

    public T getCache() {
        return (T) repo.findAll();
    }

    public void updateCache(T cache) {
        repo.save(cache);
    }

    public void invalidate() {
        repo.deleteAll();
    }
}
