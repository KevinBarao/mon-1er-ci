package com.example.demo.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Departure {

    @JsonProperty("display_informations")
    private DisplayInformations displayInformations;

    public DisplayInformations getDisplayInformations() {
        return displayInformations;
    }

    public void setDisplayInformations(DisplayInformations displayInformations) {
        this.displayInformations = displayInformations;
    }
}
