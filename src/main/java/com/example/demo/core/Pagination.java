package com.example.demo.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Pagination {

    @JsonProperty("start_page")
    private int startPage;
    @JsonProperty("items_on_page")
    private int itemsOnPage;
    private int items_per_page;
    private int total_result;

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getItemsOnPage() {
        return itemsOnPage;
    }

    public void setItemsOnPage(int itemsOnPage) {
        this.itemsOnPage = itemsOnPage;
    }

    public int getItems_per_page() {
        return items_per_page;
    }

    public void setItems_per_page(int items_per_page) {
        this.items_per_page = items_per_page;
    }

    public int getTotal_result() {
        return total_result;
    }

    public void setTotal_result(int total_result) {
        this.total_result = total_result;
    }
}