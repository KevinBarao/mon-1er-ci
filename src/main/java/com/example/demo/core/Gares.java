package com.example.demo.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Gares {

    @JsonProperty("stop_areas")
    private List<Gare> gares;

    public List<Gare> getGares() {
        return gares;
    }

    public void setGares(List<Gare> gares) {
        this.gares = gares;
    }
}
