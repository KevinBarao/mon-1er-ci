package com.example.demo.core;

import java.util.List;

public class TableauAffichage {

    private Pagination pagination;
    private List<Departure> departures;

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<Departure> getDepartures() {
        return departures;
    }

    public void setDepartures(List<Departure> departures) {
        this.departures = departures;
    }
}
