package com.example.demo.core;

public class Train {

    private String destination;
    private int numero;

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getDestination() {
        return destination;
    }

    public int getNumero() {
        return numero;
    }
}