package com.example.demo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCareController {

    @GetMapping("/test")
    public String ok() {
        return "Ok";
    }
}
