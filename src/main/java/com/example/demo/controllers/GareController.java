package com.example.demo.controllers;

import java.util.List;

import com.example.demo.core.Gare;
import com.example.demo.core.Gares;
import com.example.demo.core.TableauAffichage;
import com.example.demo.services.GareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GareController {

    @Autowired
    private GareService gareService;

    @GetMapping("/gare/{id}")
    public TableauAffichage getHours(@PathVariable("id") String idGare) {
        return gareService.getTableauDeparts(idGare);
    }

    @GetMapping("/gare")
    public Gares gare() {
        return gareService.getGares();
    }



}
