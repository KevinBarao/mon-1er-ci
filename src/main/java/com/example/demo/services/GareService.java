package com.example.demo.services;

import com.example.demo.core.Gares;
import com.example.demo.core.TableauAffichage;
import com.example.demo.core.Train;

import java.util.List;

public interface GareService {

    public TableauAffichage getTableauDeparts(String idGare);

    public Gares getGares();

}
