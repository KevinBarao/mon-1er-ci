package com.example.demo.services;

import com.example.demo.cache.CustomCache;
import com.example.demo.core.Gare;
import com.example.demo.core.Gares;
import com.example.demo.core.TableauAffichage;
import com.example.demo.repositories.GareRepository;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@Primary
public class GareServiceImpl implements GareService {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private GareRepository repo;

    @Autowired
    private CustomCache cache;
    /**
     *
     * @param url
     * @return Response : la réponse si tout va bien, null sinon
     */
    private Response apiRequest (String url) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Authorization", "BASIC MDg1OWU0NTQtMWExNi00MWJiLWJjYWItOWQwMzRmMDU5NGMyOjA4NTllNDU0LTFhMTYtNDFiYi1iY2FiLTlkMDM0ZjA1OTRjMg==")
                .url(url)
                .build();
        try {
            Response response = client.newCall(request).execute();
            return response;

        } catch (IOException e) {
            System.out.println("Erreur reseau");
            e.printStackTrace();
            return null;
        }
    }

    @Cacheable("tableauAffichage")
    public TableauAffichage getTableauDeparts(String idGare) {
        System.out.println("Perdu" +idGare);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Authorization", "BASIC MDg1OWU0NTQtMWExNi00MWJiLWJjYWItOWQwMzRmMDU5NGMyOjA4NTllNDU0LTFhMTYtNDFiYi1iY2FiLTlkMDM0ZjA1OTRjMg==")
                .url("https://api.navitia.io/v1/coverage/sncf/stop_areas/stop_area%3AOCE%3ASA%3A87393835/arrivals?from_datetime=20200303T144459&")
                .build();

        try {
            Response response = client.newCall(request).execute();
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            TableauAffichage tableauAffichage = mapper.readValue(response.body().byteStream(), TableauAffichage.class);
            return tableauAffichage;

        } catch (IOException e) {
            System.out.println("Erreur reseau");
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Gares getGares(){
        Response response = apiRequest("https://api.navitia.io/v1/coverage/sncf/stop_areas//?count=1000&");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            Gares gares = mapper.readValue(response.body().byteStream(), Gares.class);
            repo.saveAll(gares.getGares());
            cache.updateCache(gares);
            return gares;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


    }
}
