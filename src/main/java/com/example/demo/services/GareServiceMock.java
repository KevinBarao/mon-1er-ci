package com.example.demo.services;

import com.example.demo.core.Gares;
import com.example.demo.core.TableauAffichage;
import com.example.demo.core.Train;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class GareServiceMock implements GareService {

    public TableauAffichage getTableauDeparts(String idGare) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Authorization", "BASIC MDg1OWU0NTQtMWExNi00MWJiLWJjYWItOWQwMzRmMDU5NGMyOjA4NTllNDU0LTFhMTYtNDFiYi1iY2FiLTlkMDM0ZjA1OTRjMg==")
                .url("https://api.navitia.io/v1/coverage/sncf/stop_areas/stop_area%3AOCE%3ASA%3A87393835/arrivals?from_datetime=20200303T144459&")
                .build();

        try {
            Response response = client.newCall(request).execute();
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            TableauAffichage tableauAffichage = mapper.readValue(response.body().byteStream(), TableauAffichage.class);
            return tableauAffichage;

        } catch (IOException e) {
            System.out.println("Erreur reseau");
            e.printStackTrace();
        }

        return null;
    }

    public Gares getGares(){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Authorization", "BASIC MDg1OWU0NTQtMWExNi00MWJiLWJjYWItOWQwMzRmMDU5NGMyOjA4NTllNDU0LTFhMTYtNDFiYi1iY2FiLTlkMDM0ZjA1OTRjMg==")
                .url("https://api.navitia.io/v1/coverage/sncf/stop_areas//?count=1000&")
                .build();

        try {
            Response response = client.newCall(request).execute();
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Gares gares = mapper.readValue(response.body().byteStream(), Gares.class);
            return gares;

        } catch (IOException e) {
            System.out.println("Erreur reseau");
            e.printStackTrace();
        }

        return null;

    }
}
